package com.example.aws.lambda.demo.handler;


import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.example.aws.lambda.demo.model.User;
import com.example.aws.lambda.demo.response.UserResponse;
import com.example.aws.lambda.demo.util.Constant;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


import java.io.*;
import java.util.HashMap;
import java.util.Map;

//public class AwsLambdaHandler extends SpringBootRequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
//}

public class AwsLambdaHandler implements RequestStreamHandler{

    @Autowired
    AuthenticationManager authenticationManager ;


    @Override
    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

        JSONParser parser = new JSONParser();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();
        String expiresIn ;
        String token;
        String accessToken ;


        try {
            JSONObject event = (JSONObject) parser.parse(reader);

            if (event.get("body") != null) {

                User user  = new User((String) event.get("body"));
                Authentication authentication = this.authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(user.getUserName(), user.getPassword()));

                Map<String, String> credentials = new HashMap<>();
                credentials.put(Constant.PASS_WORD_KEY, user.getPassword());
                credentials.put(Constant.NEW_PASS_WORD_KEY, user.getNewPassword());

                Map<String,String> authenticatedCredentials = (Map<String, String>) authentication.getCredentials();
                token = authenticatedCredentials.get(Constant.ID_TOKEN_KEY);
                expiresIn = authenticatedCredentials.get(Constant.EXPIRES_IN_KEY);
                accessToken = authenticatedCredentials.get(Constant.ACCESS_TOKEN_KEY);

                UserResponse '`` ' userResponse = authService.getUserInfo(accessToken);

                SecurityContextHolder.getContext().setAuthentication(authentication);

            }

            JSONObject responseBody = new JSONObject();
            responseBody.put("message", "New item created");

            JSONObject headerJson = new JSONObject();
            headerJson.put("x-custom-header", "my custom header value");

            responseJson.put("statusCode", 200);
            responseJson.put("headers", headerJson);
            responseJson.put("body", responseBody.toString());

        } catch (ParseException pex) {
            responseJson.put("statusCode", 400);
            responseJson.put("exception", pex);
        }

        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toString());
        writer.close();
    }
    }
}

