package com.example.aws.lambda.demo.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class User {

    private String userName;

    private String password;

    private String newPassword;


    public User(String json) {
        Gson gson = new Gson();
        User request = gson.fromJson(json, User.class);
        this.userName = request.getUserName();
        this.password = request.getPassword();
        this.newPassword = request.getNewPassword();

    }

    public String toString() {
        final Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
